import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show get;
import '../models/station_model.dart';
import '../audio/audio_service.dart';

List<StationModel> parseStations(String responseBody) {

  //final parsed = json.decode(responseBody)['results'].cast<Map<String,dynamic>>();
  final parsed = json.decode(responseBody).cast<Map<String,dynamic>>();
  return parsed.map<StationModel>((parsedJson) => StationModel.fromJson(parsedJson)).toList();
}

class StationBloc {
  final _stations = BehaviorSubject<List<StationModel>>();
  final _selectedStation = BehaviorSubject<StationModel>();
  final _audioService = AudioService();

  StationBloc() {
    fetchStation();
    _audioService.currentItemIndex.listen((event) {
      if (event != null) {
        changeSelectedStation(_stations.value[event]);
      }
    });

  }

  Stream<List<StationModel>> get stations => _stations.stream;
  Stream<StationModel> get selectedStation => _selectedStation.stream;

  Function(List<StationModel>) get changeStations => _stations.sink.add;
  Function(StationModel) get changeSelectedStation => _selectedStation.sink.add;

  void fetchStation () async {
    final response = await get(Uri.parse("https://eaysgdyavd.execute-api.us-east-1.amazonaws.com/stations"));
    final stations = await compute(parseStations, response.body);
    changeStations(stations);
    _audioService.loadAlbum(stations);
  }
  void dispose() {
    _stations.close();
    _selectedStation.close();
  }
}

final bloc = StationBloc();