import 'package:el_meneo/src/providers/player_providers.dart';
import 'package:flutter/foundation.dart';
import 'package:just_audio/just_audio.dart';
import 'package:rxdart/rxdart.dart';
import 'package:just_audio_background/just_audio_background.dart';
import 'dart:async';
import '../models/station_model.dart';

List<AudioSource> convertAudioSource(List<StationModel> stations) {
  final List<AudioSource> audios = [];

  for ( var station in stations) {
    audios.add(
      AudioSource.uri(
        Uri.parse(station.url),
        tag: MediaItem(
          // Specify a unique ID for each media item:
          id: station.objectId,
          // Metadata to display in the notification:
          album: station.ciudad,
          title: station.name,
          artUri: Uri.parse(station.logoUrl),
        ),
      )
    );
  }
  return audios;
}

int getAudioSourceIndex(List<StationModel> stations, String objectId) {
  return stations.indexWhere((e) => e.objectId == objectId);
}
class AudioService {
  static final AudioService _singleton = AudioService._internal();
  final _player = AudioPlayer();
  late List<StationModel> playlist;
  final _currentAlbum = BehaviorSubject<StationModel>();
  final _isPlaying = BehaviorSubject<bool>();


  Stream<bool> get isPlaying => _player.playingStream;
  //Stream<StationModel> get currentItem => _currentAlbum.stream;
  Stream<int?> get currentItemIndex => _player.currentIndexStream;

  Function(StationModel) get changeCurrentStation => _currentAlbum.sink.add;

  factory AudioService() => _singleton;

  AudioService._internal(); // private constructor

  void loadAlbum(List<StationModel> stations) async {
    playlist = stations;
    final audios = await compute(convertAudioSource, stations);
    await _player.setAudioSource(
      ConcatenatingAudioSource(
        // Start loading next item just before reaching it.
        useLazyPreparation: true, // default
        // Customise the shuffle algorithm.
        shuffleOrder: DefaultShuffleOrder(), // default
        // Specify the items in the playlist.
        children: audios,
      ),
      // Playback will be prepared to start from track1.mp3
      initialIndex: 0, // default
    );
  }

  void play (StationModel station) async {
    changeCurrentStation(station);
    await _player.seek(Duration.zero, index: playlist.indexWhere((element) => element.objectId == station.objectId));
    await _player.play();
  }
  void togglePlayPause() async {
    if (_player.playerState.playing) {
      await _player.pause();
    } else {
      await _player.play();
    }
  }
  void skipNext () async {
    if (_player.hasNext) {
      await _player.seekToNext();
    }
  }
  void skipPrevious() async {
    if (_player.hasPrevious) {
      await _player.seekToPrevious();
    }
  }
  void pause () async {
    await _player.pause();
  }
  void stop() async {
    await _player.stop();
  }
  void dispose() {
    if (kDebugMode) {
      print("Terminating audio player ....");
    }
    _player.dispose();
  }
}