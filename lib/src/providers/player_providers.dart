import 'package:el_meneo/src/audio/audio_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:just_audio/just_audio.dart';

final isPlayingProvider = StreamProvider<bool>((ref) {
  final _audioService = AudioService();
  return _audioService.isPlaying;
});

// final sequenceState = StreamProvider<SequenceState?>((ref) {
//   final _audioService = AudioService();
//   _audioService.currentAudioSource.listen((event) {
//     if (kDebugMode) {
//       print("source event");
//       print(event);
//     }
//   });
//   return _audioService.currentAudioSource;
// });