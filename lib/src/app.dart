import 'package:flutter/services.dart';
import '../src/models/station_model.dart';
import '../src/widgets/station_list_widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import './bloc/station_bloc.dart';
import '../src/widgets/mini_player_widget.dart';
import './audio/audio_service.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> with WidgetsBindingObserver {
  final List<String> cities = ["Santiago", "Santo Domingo"];
  final player = AudioService();
  @override
  void initState() {
    WidgetsBinding.instance?.addObserver(this);
    super.initState();
  }
  @override
  void dispose() {
    WidgetsBinding.instance?.addObserver(this);
    super.dispose();
  }
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.detached) {
      player.dispose();
      if (kDebugMode) {
        print('state = $state');
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return MaterialApp(
      title: 'El Meneo',
      theme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: StreamBuilder(
          stream: bloc.stations,
          builder: (context, AsyncSnapshot<List<StationModel>> snapshot){
            if(snapshot.hasError) {
              if (kDebugMode) {
                print(snapshot.error);
              }
              return const Center(
                child: Text("An Error has acurred"),
              );
            } else if (snapshot.hasData) {
                return Stack(
                  children: <Widget>[
                    ListView.builder(
                    itemCount: cities.length,
                      itemBuilder: (context, index) {
                      List<StationModel> stations = snapshot.data!;
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin: const EdgeInsets.fromLTRB(10, 16, 0, 0),
                                child: Text(cities[index])
                            ),
                            SizedBox(
                              height: 180,
                              child: StationListWidget(
                                  stations.where((station) => station.ciudad == cities[index]).toList(),
                                player
                              ),
                            ),
                          ],
                        );
                      }
                    ),
                    const MiniPlayerWidget(),
            ]
                );
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }

}