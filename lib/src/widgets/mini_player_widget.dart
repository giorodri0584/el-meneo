import 'package:el_meneo/src/screens/music_player/music_player_scareen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../models/station_model.dart';
import '../bloc/station_bloc.dart';
import '../screens/player_detail/station_detail_screen.dart';
import './mini_player_play_pause_widget.dart';

class MiniPlayerWidget extends StatelessWidget {
  const MiniPlayerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<StationModel>(
      stream: bloc.selectedStation,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final StationModel station = snapshot.data!;
          return Positioned(
            bottom: 10,
            right: 5,
            left: 5,
            child: GestureDetector(
              onTap: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => MusicPlayerScreen(station)),
                // );
              },
              child: SizedBox(
                height: 70,
                child: Card(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          const SizedBox(width: 10,),
                          Image.network(station.logoUrl),
                          Text(station.name),
                        ],
                      ),
                      MiniPlayerPlayPauseWidget(),
                    ],
                  ),
                ),
              ),
            ),
          );
        } else {
          return Container();
        }
      }
    );
  }

}