import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../models/station_model.dart';
import '../screens/music_player/music_player_scareen.dart';
import '../screens/player_detail/station_detail_screen.dart';
import '../bloc/station_bloc.dart';
import '../audio/audio_service.dart';

class StationListWidget extends StatelessWidget {
  final List<StationModel> stations;
  final AudioService player;
  const StationListWidget(this.stations, this.player, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        
        physics: const ClampingScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: stations.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              bloc.changeSelectedStation(stations[index]);
              if (kDebugMode) {
                print("List Index: ${index}");
              }
              player.loadAlbum(stations);
              //player.play(index);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MusicPlayerScreen(
                    audioItem: stations[index],
                    audioIndex: index
                )
                ),
              );
            },
            child: Card(
              semanticContainer: true,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: Image.network(stations[index].logoUrl),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 5,
              margin: const EdgeInsets.all(10),
              
            ),
          );
        }
    );
  }

}