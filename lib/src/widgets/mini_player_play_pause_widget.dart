import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../audio/audio_service.dart';

class MiniPlayerPlayPauseWidget extends StatelessWidget {
  final _audioService = AudioService();

  MiniPlayerPlayPauseWidget({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _audioService.isPlaying,
        builder: (context, snapshot) {
          return Row(
            children: [
              GestureDetector(
                child: (snapshot.data == true) ?
                  const Icon(Icons.pause, size: 32,) :
                  const Icon(Icons.play_circle_filled, size: 32,),
                onTap: () {
                  if (kDebugMode) {
                    _audioService.togglePlayPause();
                  }
                },
              ),
              const SizedBox(width: 10,),
            ],
          );
        }
    );
  }
  
}