class StationModel {
  final String objectId;
  final String name;
  final String frequency;
  final bool isWorking;
  final String ciudad;
  final String url;
  final String logoUrl;
  final String region;
  const StationModel({
    required this.objectId,
    required this.name,
    required this.frequency,
    required this.isWorking,
    required this.ciudad,
    required this.url,
    required this.logoUrl,
    required this.region
  });
  factory StationModel.fromJson(Map<String,dynamic> parsedJson) {
    return StationModel (
      objectId: parsedJson['objectId'],
      name: parsedJson['name'],
      frequency: parsedJson['frequency'],
      isWorking: parsedJson['isWorking'],
      ciudad: parsedJson['ciudad'],
      url: parsedJson['url'],
      logoUrl: parsedJson['logoUrl'],
      region: parsedJson['region']
    );

  }
}
