import 'package:flutter/material.dart';

class StationDetailSliderWidget extends StatelessWidget {
  const StationDetailSliderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 5, right: 5),
      width: double.infinity,
      child: SliderTheme(
        data: SliderTheme.of(context).copyWith(
          activeTrackColor: Colors.white,
          inactiveTrackColor: Colors.grey.shade600,
          activeTickMarkColor: Colors.white,
          thumbColor: Colors.white,
          trackHeight: 3,
          thumbShape: const RoundSliderThumbShape(
            enabledThumbRadius: 4,
          ),
        ),
        child: Slider(
          value: 80,
          min: 0,
          max: 1000,
          onChanged: (double value) {
            // setState(() {
            //   seekToSecond(value.toInt());
            //   value = value;
            // });
          },
        ),
      ),
    );
  }

}