import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class StationDetailTopBar extends StatelessWidget {
  final String stationName;
  const StationDetailTopBar(this.stationName, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        IconButton(
          icon: const Icon(
            LineIcons.angleLeft, //LineIcons.angleDown
            color: Colors.white,
            size: 24,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        Column(
          children: <Widget>[
            const Text(
              "TOCANDO EMISORA",
              style: TextStyle(
                letterSpacing: 1,
                fontSize: 11,
                color: Colors.white,
              ),
            ),
            Text(
              stationName,
              style: const TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.bold,
                letterSpacing: 0.5,
                fontFamily: "ProximaNova",
                color: Colors.white,
              ),
            ),
          ],
        ),
        const Icon(
          LineIcons.verticalEllipsis,
          color: Colors.white,
          size: 24,
        )
      ],
    );
  }

}