import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class StationDetailTitleHeartWidget extends StatelessWidget {
  final String stationName;
  final String stationCiudad;
  const StationDetailTitleHeartWidget(
      this.stationName,
      this.stationCiudad, {Key? key}
      ) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 25, right: 25),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                stationName,
                style: const TextStyle(
                  color: Colors.white,
                  fontFamily: "ProximaNova",
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  wordSpacing: 0.2,
                ),
              ),
              Text(
                stationCiudad,
                style: TextStyle(
                  color: Colors.grey.shade400,
                  fontFamily: "ProximaNovaThin",
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  letterSpacing: 0.1,
                ),
              ),
            ],
          ),
          // IconButton(
          //   icon: (true == true)
          //       ? const Icon(
          //     LineIcons.heart,
          //     color: Colors.green,
          //     size: 30,
          //   )
          //       : Icon(
          //     LineIcons.heart,
          //     color: Colors.grey.shade400,
          //     size: 30,
          //   ),
          //   onPressed: () {
          //     // setState(() {
          //     //   isHeartPressed =
          //     //   (isHeartPressed == false) ? true : false;
          //     // });
          //   },
          // ),
        ],
      ),
    );
  }

}