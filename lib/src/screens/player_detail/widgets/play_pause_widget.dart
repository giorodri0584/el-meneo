import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../../../audio/audio_service.dart';

class PlayPauseWidget extends StatelessWidget {
  final AudioService audioService;
  const PlayPauseWidget( this.audioService, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: audioService.isPlaying,
        builder: (context, snapshot) {
          return SizedBox(
            height: 90,
            width: 90,
            child: Center(
              child: IconButton(
                  iconSize: 70,
                  alignment: Alignment.center,
                  icon: (snapshot.data == true)
                    ? const Icon(
                        Icons.pause_circle_filled,
                        color: Colors.white,
                      )
                    : const Icon(
                        Icons.play_circle_filled,
                        color: Colors.white,
                      ),
                  onPressed: () {
                    audioService.togglePlayPause();
                  }
                  ),
              ),
            );
        }
    );
  }
}