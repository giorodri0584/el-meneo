import 'package:el_meneo/src/screens/player_detail/widgets/duration_widget.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../audio/audio_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:line_icons/line_icons.dart';
import '../../models/station_model.dart';
import './widgets/play_pause_widget.dart';
import './widgets/station_detail_top_bar.dart';
import './widgets/station_detail_title_heart_widget.dart';
import './widgets/station_detail_slider_widget.dart';

class StationDetailScreen extends StatefulWidget {
  final StationModel station;
  const StationDetailScreen(this.station, {Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return StationDetailScreenState();
  }

}


class StationDetailScreenState extends State<StationDetailScreen> {
  final AudioService audioService = AudioService();
  bool isHeartPressed = false;
  bool isPlayPressed = false;

  // double _value = 0;
  final Duration _duration = new Duration();
  final Duration _position = new Duration();
  //AudioPlayer advancedPlayer;
  //AudioCache audioCache;

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return Scaffold(

      body: Container(
        padding: const EdgeInsets.only(
          left: 10,
          right: 10,
          top: 40,
        ),
        // decoration: const BoxDecoration(
        //   gradient: LinearGradient(
        //     begin: Alignment.topCenter,
        //     colors: [
        //       Colors.brown,
        //       Colors.black87,
        //     ],
        //     end: Alignment.bottomCenter,
        //   ),
        // ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              StationDetailTopBar(widget.station.name),
              // const SizedBox(
              //   height: 100,
              // ),
              const Spacer(),
              SizedBox(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                      widget.station.logoUrl),
                ),
                width: 325,
              ),
              // const SizedBox(
              //   height: 100,
              // ),
              const Spacer(),
              StationDetailTitleHeartWidget(widget.station.name, widget.station.ciudad),
              const Spacer(),
              //DurationWidget(position: _position, duration: _duration),
              Container(
                padding: const EdgeInsets.only(left: 22, right: 22),
                width: double.infinity,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    // Icon(
                    //   LineIcons.random,
                    //   color: Colors.grey.shade400,
                    // ),
                    IconButton(
                    iconSize: 40,
                      onPressed: () {
                        audioService.skipPrevious();
                      },
                        icon: const Icon(
                          Icons.skip_previous,
                          color: Colors.white,
                        )
                    ),
                    PlayPauseWidget(audioService),
                    IconButton(
                        iconSize: 40,
                        onPressed: () {
                          audioService.skipNext();
                        },
                        icon: const Icon(
                          Icons.skip_next,
                          color: Colors.white,
                        )
                    ),
                    // Icon(
                    //   LineIcons.redo,
                    //   color: Colors.grey.shade400,
                    // ),
                  ],
                ),
              ),
              // Container(
              //   padding: const EdgeInsets.only(left: 22, right: 22),
              //   width: double.infinity,
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: <Widget>[
              //       Icon(
              //         LineIcons.desktop,
              //         color: Colors.grey.shade400,
              //       ),
              //       Icon(
              //         LineIcons.alternateList,
              //         color: Colors.grey.shade400,
              //       ),
              //     ],
              //   ),
              // ),
              const SizedBox(
                height: 40,
              )
            ],
          ),
        ),
      ),
    );
  }
}