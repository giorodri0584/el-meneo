import 'package:flutter/material.dart';

class AudioImageWidget extends StatelessWidget {
  final logoUrl;
  const AudioImageWidget({
    @required this.logoUrl,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 15),
        child: Card(
          elevation: 5,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: Image.network(
              logoUrl,
              fit: BoxFit.fill,
              width: 300,
              height: 300,
            ),
          ),
        )
    );
  }
}