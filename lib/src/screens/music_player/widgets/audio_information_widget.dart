import 'package:flutter/material.dart';

import '../../../models/station_model.dart';

class AudioInformationWidget extends StatelessWidget {
  const AudioInformationWidget({
    Key? key,
    required this.selectedAudio,
  }) : super(key: key);

  final StationModel selectedAudio;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text(
            "En Vivo",
            style: TextStyle(
                fontSize:  20,
                color: Colors.white
            )
        ),
        const SizedBox(height: 10,),
        Text(selectedAudio.name),
        Text("${selectedAudio.ciudad} - ${selectedAudio.frequency}")
      ],
    );
  }
}