import 'package:el_meneo/src/audio/audio_service.dart';
import 'package:el_meneo/src/providers/player_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class PlayPauseWidget extends ConsumerWidget {
  final _audioPlayer = AudioService();
  final int audioIndex;

  PlayPauseWidget({required this.audioIndex, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isPlaying = ref.watch(isPlayingProvider);
    return isPlaying.when(
        data: (data) => IconButton(
          icon: data ? Icon(Icons.pause_circle,) : Icon(Icons.play_circle,),
          iconSize: 55,
          onPressed: () => _audioPlayer.togglePlayPause(),
          hoverColor: Colors.transparent,
        ),
        error: (err, stack) => Text('Error $err'),
        loading: () => const Text("Loading")
    );
  }

}