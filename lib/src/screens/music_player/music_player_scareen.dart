import 'package:el_meneo/src/audio/audio_service.dart';
import 'package:el_meneo/src/screens/music_player/widgets/audio_image_widget.dart';
import 'package:el_meneo/src/screens/music_player/widgets/audio_information_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../models/station_model.dart';
import '../../providers/player_providers.dart';
import '../music_player/widgets/play_pause_widget.dart';

final selectedAudioProvider = StateProvider<StationModel>((ref) {
  return const StationModel(
      objectId: "zojekNqeOk",
      name: "La Bakana",
      logoUrl: "https://static-media.streema.com/media/cache/40/33/4033faa50265d53220e5ba1d9c0855a0.jpg",
    frequency: "105.9",
    ciudad: "Santiago",
    isWorking: true,
    region: "Norte",
    url: "https://radio4.domint.net:8028/stream"
  );
});

class MusicPlayerScreen extends ConsumerWidget {
  final StationModel audioItem;
  final int audioIndex;
  const MusicPlayerScreen({required this.audioItem, required this.audioIndex, Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context, WidgetRef ref) {

    final _audioPlayer = AudioService();
    return Scaffold(
        body: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const SizedBox(height: 16,),
                Row(
                  children: [
                    IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: Icon(Icons.arrow_back)
                    )
                  ],
                ),
                AudioImageWidget(logoUrl: audioItem.logoUrl,),
                const SizedBox(height: 50,),
                AudioInformationWidget(selectedAudio: audioItem),
                const SizedBox(height: 50,),
                Container(
                  padding: const EdgeInsets.only(left: 32, right: 32),
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      const Icon(
                          Icons.skip_previous,
                        size: 40,
                      ),
                      PlayPauseWidget(audioIndex: audioIndex,),
                      const Icon(
                          Icons.skip_next,
                        size: 40,
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 50,),
              ],
            ),
          ),
        ),
    );
  }

}