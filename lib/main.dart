import 'package:el_meneo/src/screens/music_player/music_player_scareen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import './src/app.dart';
import 'package:just_audio_background/just_audio_background.dart';

void main () async {
  await JustAudioBackground.init(
    androidNotificationChannelId: 'com.rodriguez.giomar.elmeneo.channel.audio',
    androidNotificationChannelName: 'Audio playback',
    androidNotificationOngoing: true,
  );
  // runApp(
  //   const ProviderScope(child: App())
  // );

  runApp(
    const ProviderScope(child: App())
  );

}